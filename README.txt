Пути захардкожены, поэтому содержимое репозитория должно находиться в /home/shredfs/python-fuse-sample
В /etc/fuse.conf должны быть раскомментирован "user_allow_other"

Для python3 должен быть установлен pycryptodome.
Разрабатывалось на debian 11


./a1_mount_keys_and_data.sh:	Запускает файловую систему
./a2_umount_all.sh:		Останавливает работу файловой системы

./a3_rotate_keys.sh:		Чистит все ключи от удалённых файлов, затем переносит их
				в новый криптоконтейнер

Стандартный пароль: '


Установка:
apt install python3-pycryptodome encfs pip fuse  # либо fuse3
pip3 install -r requirements.txt
