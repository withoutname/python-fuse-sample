#!/bin/bash

mkdir -p ./data

./a2_umount_all.sh

rm ./keys/* 2>/dev/null
encfs /home/shredfs/python-fuse-sample/enc_keys/ /home/shredfs/python-fuse-sample/keys/ && \
echo "Подмонтировалось хранилище ключей, сейчас будет подмонтировано хранилище данных, без оповещения" && \
python3 passthrough.py /home/shredfs/python-fuse-sample/enc_data/ /home/shredfs/python-fuse-sample/data/
