#!/bin/bash

fusermount -u /home/shredfs/python-fuse-sample/keys
echo "Отмонтировали хранилище ключей"

fusermount -u /home/shredfs/python-fuse-sample/data
echo "Отмонтировали хранилище файлов"
