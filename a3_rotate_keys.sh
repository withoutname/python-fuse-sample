#!/bin/bash

rm -rf /dev/shm/.rotate_keys
mkdir -p tmp
mkdir -p /dev/shm/.rotate_keys
for i in data/*; do
  a="$(basename "$i")"
  #echo "${a}.key"
  cp keys/"${a}.key" /dev/shm/.rotate_keys/
  mv enc_data/"${a}" tmp/
done

rm -rf enc_data/
mv tmp/ enc_data/

./a2_umount_all.sh
rm -rf ./enc_keys/*
rm -rf ./enc_keys/.*
rm -rf ./keys/*
rm -rf ./keys/.*

echo "'" | encfs --standard --stdinpass /home/shredfs/python-fuse-sample/enc_keys/ /home/shredfs/python-fuse-sample/keys/ || { echo "Ошибка при создании хранилища ключей" ; exit 1; }
#encfs --standard /home/shredfs/python-fuse-sample/enc_keys/ /home/shredfs/python-fuse-sample/keys/ || { echo "Ошибка при создании хранилища ключей" ; exit 1; }

cp -rv /dev/shm/.rotate_keys/* ./keys/
rm -rf /dev/shm/.rotate_keys/ /dev/shm/*
echo "Ротация ключей завершена, актуальные ключи скопированы в хранилище ключей"

echo "Подмонтирование хранилища файлов, без уведомления"
python3 passthrough.py /home/shredfs/python-fuse-sample/enc_data/ /home/shredfs/python-fuse-sample/data/
