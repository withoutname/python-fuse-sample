#!/usr/bin/env python3

from __future__ import with_statement

import os
import sys
import errno

from fuse import FUSE, FuseOSError, Operations, fuse_get_context
from python_libs import Padding
from Cryptodome.Cipher import AES
import secrets
import subprocess
import glob




def genkey(path):
    if os.path.exists(path): return
    with open(path, "w+b") as file:
        file.write(secrets.token_bytes(32))

def encrypt(key, data):
    aes = AES.new(key, AES.MODE_CBC, bytes(1)*16)
    result = aes.encrypt(Padding.pad(data, 16))
    return result

def decrypt(key, data):
    aes = AES.new(key, AES.MODE_CBC, bytes(1)*16)
    result = Padding.unpad(aes.decrypt(data),16)
    return result

def unencrypt_all(encdir, dir):
    #print(encdir, dir)
    files = glob.glob(encdir + '*')
    for i in files:
         #print(os.path.dirname(i))
         with open(i, "r+b") as f: data = f.read()
         with open(os.path.dirname(i) + '/../keys/' + os.path.basename(i) + '.key' , "r+b") as k: key = k.read()
         with open(dir + os.path.basename(i), "w+b") as f2:
             f2.write(decrypt(key, data))




class Passthrough(Operations):
    def __init__(self, root):
        self.root = root
        #sr = self.root + '/*'
        #subprocess.run(['sh', '-c', f'cp -rv {sr} /dev/shm'])
        unencrypt_all(self.root, '/dev/shm/')


    # Encryption
    # ==========




    # Helpers
    # =======

    def _shm_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join('/dev/shm/', partial)
        #return '/dev/shm/tmpfile.shredfs'
        return path

    def _full_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join('/dev/shm', partial)
        return path

    def _real_full_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join(self.root, partial)
        return path


    def _key_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:] + '.key'
        path = os.path.join((self.root + '../keys/'), partial)
        return path

    # Filesystem methods
    # ==================

    def access(self, path, mode):
        full_path = self._full_path(path)
        if not os.access(full_path, mode):
            raise FuseOSError(errno.EACCES)

    def chmod(self, path, mode):
        full_path = self._full_path(path)
        return os.chmod(full_path, mode)

    def chown(self, path, uid, gid):
        full_path = self._full_path(path)
        return os.chown(full_path, uid, gid)

    def getattr(self, path, fh=None):
        full_path = self._full_path(path)
        st = os.lstat(full_path)
        return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                     'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))

    def readdir(self, path, fh):
        full_path = self._full_path(path)

        dirents = ['.', '..']
        if os.path.isdir(full_path):
            dirents.extend(os.listdir(full_path))
        for r in dirents:
            yield r

    def readlink(self, path):
        pathname = os.readlink(self._full_path(path))
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    def mknod(self, path, mode, dev):
        return os.mknod(self._full_path(path), mode, dev)

    def rmdir(self, path):
        full_path = self._full_path(path)
        return os.rmdir(full_path)

    def mkdir(self, path, mode):
        return os.mkdir(self._full_path(path), mode)

    def statfs(self, path):
        full_path = self._full_path(path)
        stv = os.statvfs(full_path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))

    def unlink(self, path):
        return os.unlink(self._full_path(path))

    def symlink(self, name, target):
        return os.symlink(target, self._full_path(name))

    def rename(self, old, new):
        return os.rename(self._full_path(old), self._full_path(new))

    def link(self, target, name):
        return os.link(self._full_path(name), self._full_path(target))

    def utimens(self, path, times=None):
        return os.utime(self._full_path(path), times)

    # File methods
    # ============

    def open(self, path, flags):
        full_path = self._full_path(path)
        return os.open(full_path, flags)

    def create(self, path, mode, fi=None):
        uid, gid, pid = fuse_get_context()
        full_path = self._full_path(path)
        keyname = self._key_path(path)
        #print(keyname)
        #keyname = path + '.key'
        #if keyname.startswith("/"):
        #    keyname = keyname[1:]
        #print(keyname)
        genkey(keyname)
        #genkey(str('/home/shredfs/python-fuse-sample/keys/' + keyname))
        #genkey('/home/shredfs/python-fuse-sample/keys/kkk.key')
        fd = os.open(full_path, os.O_WRONLY | os.O_CREAT, mode)
        os.chown(full_path,uid,gid) #chown to context uid & gid
        return fd

    def read(self, path, length, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        return os.read(fh, length)

    def write(self, path, buf, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        genkey(self._key_path(path))
        with open(self._key_path(path), "r+b") as f: key = f.read()
        os.write(fh, buf)
        #print(self._full_path(path), self._real_full_path(path))
        #subprocess.run(['cp', self._full_path(path), self._real_full_path(path)])
        with open(self._full_path(path), 'r+b') as t: to_enc = t.read()
        with open(self._real_full_path(path), 'w+b') as f:
            f.write(encrypt(key, to_enc))
        return True # os.write(fh, buf)
        #full_path = self._full_path(path)
        #with open(full_path, 'r+b') as f:
        #    return f.write(buf)

    def truncate(self, path, length, fh=None):
        full_path = self._full_path(path)
        with open(full_path, 'r+') as f:
            f.truncate(length)

    def flush(self, path, fh):
        return os.fsync(fh)

    def release(self, path, fh):
        return os.close(fh)

    def fsync(self, path, fdatasync, fh):
        return self.flush(path, fh)


def main(mountpoint, root):
    FUSE(Passthrough(root), mountpoint, nothreads=True, foreground=True, allow_other=True)


if __name__ == '__main__':
    main(sys.argv[2], sys.argv[1])
